package com.example.demo.service;

import com.example.demo.config.PathConfig;
import com.example.demo.dao.model.Image;
import com.example.demo.dao.repository.ImageRepository;
import com.example.demo.dto.AuthDto;
import com.example.demo.dto.ImageDto;
import com.example.demo.dto.PageableImageDto;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.util.Param;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FetchServiceImpl implements FetchService {

  private final HttpClient client = HttpClient.newHttpClient();
  private final Gson gson = new Gson();
  private final ImageRepository imageRepository;

  private static final String TOKEN_NAME = "Bearer";

  @Autowired
  public FetchServiceImpl(ImageRepository imageRepository) {
    this.imageRepository = imageRepository;
  }

  @Override
  @Scheduled(fixedDelay = 100000, initialDelay = 1)
  public void parseImages() throws IOException, InterruptedException {
    log.info("Start parsing");
    Integer pageCount = fetchPageCount();

    Param param = Param.builder()
        .key("page")
        .build();

    List<String> imageIds;
    for (int i = 1; i <= pageCount; i++) {
      param.setValue(i);
      PageableImageDto pageableImageDto = fetchImagePage(param);

      imageIds = pageableImageDto.getPictures().stream().map(ImageDto::getId).collect(Collectors.toList());

      List<Image> images = fetchImagesById(imageIds);
      imageRepository.saveAll(images);
    }
    log.info("End parsing");
  }

  @Override
  public PageableImageDto fetchImagePage(Param pageParam) throws IOException, InterruptedException {
    HttpRequest request;
    HttpResponse<String> response;
    AuthDto authDto = fetchAuthToken();

    request = HttpRequest.newBuilder()
        .uri(URI.create(PathConfig.IMAGES_URI + "?" + pageParam))
        .header("Authorization", TOKEN_NAME + " " + authDto.getToken())
        .build();

    response = client.send(request, HttpResponse.BodyHandlers.ofString());
    return gson.fromJson(response.body(), PageableImageDto.class);
  }

  @Override
  public List<Image> fetchImagesById(List<String> ids) throws IOException, InterruptedException {
    AuthDto authDto = fetchAuthToken();

    List<Image> responseList = new LinkedList<>();

    for (String id : ids) {
      HttpRequest request = HttpRequest.newBuilder()
          .uri(URI.create(PathConfig.IMAGES_URI + "/" + id))
          .header("Authorization", TOKEN_NAME + " " + authDto.getToken())
          .build();

      HttpResponse<String> response =
          client.send(request, HttpResponse.BodyHandlers.ofString());

      responseList.add(ImageServiceImpl.dtoToEntity.apply(gson.fromJson(response.body(), ImageDto.class))); //sorry for this)
    }
    return responseList;
  }

  @Override
  public Integer fetchPageCount() throws IOException, InterruptedException {
    AuthDto auth = fetchAuthToken();

    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(PathConfig.IMAGES_URI))
        .header("Authorization", TOKEN_NAME + " " + auth.getToken())
        .build();

    HttpResponse<String> response =
        client.send(request, HttpResponse.BodyHandlers.ofString());

    PageableImageDto pageableImageDto = gson.fromJson(response.body(), PageableImageDto.class);
    return pageableImageDto.getPageCount();
  }

  @Override
  public AuthDto fetchAuthToken() throws IOException, InterruptedException {
    String body = "{ \"apiKey\": \"23567b218376f79d9415\" }";

    HttpRequest request = HttpRequest.newBuilder()
        .uri(URI.create(PathConfig.AUTH_URI))
        .header("Content-Type", "application/json")
        .POST(HttpRequest.BodyPublishers.ofString(body))
        .build();

    HttpResponse<String> response =
        client.send(request, HttpResponse.BodyHandlers.ofString());

    return gson.fromJson(response.body(), AuthDto.class);
  }

}
