package com.example.demo.service;

import com.example.demo.dao.model.Image;
import com.example.demo.dto.AuthDto;
import com.example.demo.dto.ImageDto;
import com.example.demo.dto.PageableImageDto;
import com.example.demo.util.Param;

import java.io.IOException;
import java.util.List;

public interface FetchService {
  void parseImages() throws IOException, InterruptedException;

  List<Image> fetchImagesById(List<String> ids) throws IOException, InterruptedException;

  PageableImageDto fetchImagePage(Param pageParam) throws IOException, InterruptedException;

  Integer fetchPageCount() throws IOException, InterruptedException;

  AuthDto fetchAuthToken() throws IOException, InterruptedException;

}
