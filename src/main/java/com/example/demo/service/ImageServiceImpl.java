package com.example.demo.service;

import com.example.demo.dao.model.Image;
import com.example.demo.dao.repository.ImageRepository;
import com.example.demo.dto.ImageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ImageServiceImpl implements ImageService {

  private final ImageRepository imageRepository;

  @Autowired
  public ImageServiceImpl(ImageRepository imageRepository) {
    this.imageRepository = imageRepository;
  }

  @Override
  public List<ImageDto> searchByTerm(String searchTerm) {
    return imageRepository.findAllBySearchTerm(searchTerm)
        .stream()
        .map(entityToDto)
        .collect(Collectors.toList());
  }

  public static Function<ImageDto, Image> dtoToEntity = dto -> Image.builder()
      .author(dto.getAuthor())
      .camera(dto.getCamera())
      .croppedPicture(dto.getCropped_picture())
      .fullPicture(dto.getFull_picture())
      .id(dto.getId())
      .tags(dto.getTags())
      .build();

  private Function<Image, ImageDto> entityToDto = entity -> ImageDto.builder()
      .author(entity.getAuthor())
      .camera(entity.getCamera())
      .cropped_picture(entity.getCroppedPicture())
      .full_picture(entity.getFullPicture())
      .id(entity.getId())
      .tags(entity.getTags())
      .build();
}
