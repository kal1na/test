package com.example.demo.service;

import com.example.demo.dto.ImageDto;

import java.util.List;

public interface ImageService {
  List<ImageDto> searchByTerm(String searchTerm);
}
