package com.example.demo.dao.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * id": "1a5e86953ad5ac438130",
 *     "author": "Cool Respond",
 *     "camera": "Pentax 645D",
 *     "tags": "#wonderfulday #wonderfullife #greatview #photooftheday #life #whataview ",
 *     "cropped_picture": "http://interview.agileengine.com/pictures/cropped/0002.jpg",
 *     "full_picture": "http://interview.agileengine.com/pictures/full_size/0002.jpg"
 * }
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Image {
  @Id
  private String id;
  private String author;
  private String camera;
  private String tags;
  private String croppedPicture;
  private String fullPicture;

}
