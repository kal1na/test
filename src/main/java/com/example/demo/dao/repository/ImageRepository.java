package com.example.demo.dao.repository;

import com.example.demo.dao.model.Image;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ImageRepository extends CrudRepository<Image, String> {
  @Query(
      "select im from Image im " +
          "WHERE im.author = :searchTerm " +
          "OR im.camera = :searchTerm " +
          "OR im.id = :searchTerm " +
          "OR im.tags like %:searchTerm% "
  )
  List<Image> findAllBySearchTerm(@Param("searchTerm") String searchTerm);

}
