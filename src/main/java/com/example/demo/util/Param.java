package com.example.demo.util;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Builder
@Setter
public class Param {
  private String key;
  private Integer value;

  @Override
  public String toString() {
    return key + "=" + value;
  }
}
