package com.example.demo.controller;

import com.example.demo.dao.model.Image;
import com.example.demo.dao.repository.ImageRepository;
import com.example.demo.dto.ImageDto;
import com.example.demo.service.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class ImageController {

  private final ImageService imageService;

  @Autowired
  public ImageController(ImageService imageService) {
    this.imageService = imageService;
  }

  @GetMapping("/search")
  public List<ImageDto> getBySearchTerm(@RequestParam String searchTerm) {
    log.info("GET images by search term: {}", searchTerm);
    return imageService.searchByTerm(searchTerm);
  }
}
