package com.example.demo.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PageableImageDto {
  private List<ImageDto> pictures;

  private Integer page;

  private Integer pageCount;

  private Boolean hasMore;
}
