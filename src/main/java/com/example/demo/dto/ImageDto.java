package com.example.demo.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ImageDto {
  private String id;
  private String author;
  private String camera;
  private String tags;
  private String cropped_picture;
  private String full_picture;
}

/**
 * {
 *     "id": "c0d2f9fe5f35132bd336",
 *     "author": "Rotten Elevator",
 *     "camera": "Nikon D700",
 *     "tags": "#photography #natureisbeautiful ",
 *     "cropped_picture": "http://interview.agileengine.com/pictures/cropped/01sc087.jpg",
 *     "full_picture": "http://interview.agileengine.com/pictures/full_size/01sc087.jpg"
 * }
 */
