package com.example.demo.config;


public class PathConfig {
  public static String API_URI = "http://interview.agileengine.com";
  public static String AUTH_URI = API_URI +"/auth";
  public static String IMAGES_URI = API_URI + "/images";
}
